<?php
$name = $email = $website = $comment =$gender ="";
$errname = $erremail = $errwebsite = $errgender = "";


if ($_SERVER["REQUEST_METHOD"]=="POST"){
    if (empty($_POST["name"])){
        $errname = "<span style='color: red'>Name is required.</span>";
    }else {
        $name = validate($_POST["name"]);
    }
    if (empty($_POST["email"])){
        $erremail = "<span style='color: red'>E-mail is required.</span>";
    }elseif (!filter_var(($_POST["email"]),FILTER_VALIDATE_EMAIL)){
        $erremail = "<span style='color: red'>Invalid E-mail format.</span>";
    }else {
        $email   = validate($_POST["email"]);
    }
    if (empty($_POST["website"])){
        $errwebsite = "<span style='color: red'>Website is required.</span>";
    }elseif (!filter_var(($_POST["website"]),FILTER_VALIDATE_URL)){
        $errwebsite = "<span style='color: red'>Invalid URL format.</span>";
    }else {
        $website = validate($_POST["website"]);
    }
    if (empty($_POST["gender"])){
        $errgender = "<span style='color: red'>Gender is required.</span>";
    }else {
        $gender  = validate($_POST["gender"]);
    }
    $comment = validate($_POST["comment"]);


}
function validate($data){
    $data = trim($data);
    $data = stripcslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}
?>

<!Doctype html>
<html>

<head>
    <title>PHP Syntax</title>
    <style>

        body{font-family: 'Open Sans', sans-serif }
        .phpcoding{width: 900px; margin: 0 auto; background: <?php echo " #ddddd8";?> ;
            min-height: 400px;}
        .headeroption  {
            background: #00bf00;
            color: #0000bf ; text-align: center; padding: 20px;
        }
        .footeroption{
            background: orange ;
            color: #2a3133 ; text-align: center; border-bottom-width: 200px;
        }
        .headeroption h2{margin: 0;}
        .footeroption h2{
            margin: 0;}
        .maincontent{min-height: 400px;padding: 20px;}
    </style>
</head>

<body>
<div class="phpcoding">
    <section class="headeroption">
        <h2><?php echo "PHP Fundamental Training";?></h2>
    </section>
    <section class="maincontent">

        <hr/>
        PHP Form Validation
        <hr/>

        <form method="post" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']);?>">
            <table>
                <p style="color: red">* Required field</p>
                <tr>
                    <td>Name:</td>
                    <td><input type="text" name="name"/>*<?php echo $errname?></td>
                </tr>
                <tr>
                    <td>E-Mail:</td>
                    <td><input type="text" name="email"/>*<?php echo $erremail?></td>
                </tr>
                <tr>
                    <td>Website:</td>
                    <td><input type="text" name="website"/>*<?php echo $errwebsite?></td>
                </tr>
                <tr>
                    <td>Comment:</td>
                    <td><textarea name="comment" rows="5" cols="40"></textarea></td>
                </tr>
                <tr>
                    <td>Gender</td>
                    <td>
                        <input type="radio" name="gender" value="male"/>Male
                        <input type="radio" name="gender" value="female"/>Female
                        *<?php echo $errgender?></td>
                </tr>
                <tr>
                    <td></td>
                    <td><input type="submit" name="submit" value="submit"/></td>
                </tr>
            </table>

        </form>

    </section>
    <section class="footeroption">
        <h2>www.trainingwithliveprojects.com</h2>
    </section>

</div>
</body>
</html>

