<!Doctype html>
<html>

<head>
    <title>PHP Syntax</title>
    <style>

        body{font-family: 'Open Sans', sans-serif }
        .phpcoding{width: 900px; margin: 0 auto; background: <?php echo " #ddddd8";?> ;
            min-height: 400px;}
        .headeroption  {
            background: #00bf00;
            color: #0000bf ; text-align: center; padding: 20px;
        }
        .footeroption{
            background: orange ;
            color: #2a3133 ; text-align: center; border-bottom-width: 200px;
        }
        .headeroption h2{margin: 0;}
        .footeroption h2{
            margin: 0;}
        .maincontent{min-height: 400px;padding: 20px;}
    </style>
</head>

<body>
<div class="phpcoding">
    <section class="headeroption">
        <h2><?php echo "PHP Fundamental Training";?></h2>
    </section>
    <section class="maincontent">

        <hr/>
        PHP Superglobals [$GET]
        <hr/>

        <a href="text.php?msg=Good&txt=Bye">Sent Data</a>


        <?php

        ?>
    </section>
    <section class="footeroption">
        <h2>www.trainingwithliveprojects.com</h2>
    </section>

</div>
</body>
</html>

