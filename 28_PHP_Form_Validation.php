<!Doctype html>
<html>

<head>
    <title>PHP Syntax</title>
    <style>

        body{font-family: 'Open Sans', sans-serif }
        .phpcoding{width: 900px; margin: 0 auto; background: <?php echo " #ddddd8";?> ;
            min-height: 400px;}
        .headeroption  {
            background: #00bf00;
            color: #0000bf ; text-align: center; padding: 20px;
        }
        .footeroption{
            background: orange ;
            color: #2a3133 ; text-align: center; border-bottom-width: 200px;
        }
        .headeroption h2{margin: 0;}
        .footeroption h2{
            margin: 0;}
        .maincontent{min-height: 400px;padding: 20px;}
    </style>
</head>

<body>
<div class="phpcoding">
    <section class="headeroption">
        <h2><?php echo "PHP Fundamental Training";?></h2>
    </section>
    <section class="maincontent">

        <hr/>
        PHP Form Validation
        <hr/>

        <form method="post" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']);?>">
        <table>
            <tr>
                <td>Name:</td>
                <td><input type="text" name="name" required/></td>
            </tr>
            <tr>
                <td>E-Mail:</td>
                <td><input type="text" name="email"/></td>
            </tr>
            <tr>
                <td>Website:</td>
                <td><input type="text" name="website"/></td>
            </tr>
            <tr>
                <td>Comment:</td>
                <td><textarea name="comment" rows="5" cols="40"></textarea></td>
            </tr>
            <tr>
                <td>Gender</td>
                <td>
                <input type="radio" name="gender" value="male"/>Male
                <input type="radio" name="gender" value="female"/>Female
                </td>
            </tr>
            <tr>
                <td></td>
                <td><input type="submit" name="submit" value="submit"/></td>
            </tr>
        </table>

        </form>

        <?php
            $name = $email = $website = $comment =$gender ="";

            if ($_SERVER["REQUEST_METHOD"]=="POST"){
                $name    = validate($_POST["name"]);
                $email   = validate($_POST["email"]);
                $website = validate($_POST["website"]);
                $comment = validate($_POST["comment"]);
                $gender  = validate($_POST["gender"]);

                echo "Name:".$name."<br/>";
                echo "E-mail:".$email."<br/>";
                echo "Website:".$website."<br/>";
                echo "Comment:".$comment."<br/>";
                echo "Gender:".$gender;
            }
            function validate($data){
                $data = trim($data);
                $data = stripcslashes($data);
                $data = htmlspecialchars($data);
                return $data;
            }

        ?>
    </section>
    <section class="footeroption">
        <h2>www.trainingwithliveprojects.com</h2>
    </section>

</div>
</body>
</html>

